# flutter_demo_01

Một ứng dụng Flutter mới.

## Bắt đầu

Dự án này là điểm khởi đầu cho một ứng dụng Flutter.

Một vài tài nguyên để giúp bạn bắt đầu nếu đây là dự án Flutter đầu tiên của bạn:

- [Lab: Viết ứng dụng Flutter đầu tiên của bạn](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Mẫu Flutter hữu ích](https://flutter.dev/docs/cookbook)

Để được trợ giúp bắt đầu với Flutter, hãy xem:
[tài liệu trực tuyến](https://flutter.dev/docs), trong đó cung cấp hướng dẫn,
các mẫu, hướng dẫn về phát triển di động và tham chiếu API đầy đủ.
