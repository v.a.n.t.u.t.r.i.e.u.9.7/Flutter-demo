import 'dart:async';

import 'package:flutter_demo_01/validations/auth_validations.dart';

class AuthBloc {

  StreamController _userNameController = new StreamController();
  StreamController _passwordController = new StreamController();
  StreamController _phoneController = new StreamController();
  StreamController _emailController = new StreamController();

  Stream get userNameStream => _userNameController.stream;

  Stream get passwordStream => _passwordController.stream;

  Stream get phoneStream => _phoneController.stream;

  Stream get emailStream => _emailController.stream;

  bool isSignUpValid(
      String email, String password, String phone, String userName) {
    print('(sign_up_bloc.dart) _isSignUpValid  .|.  Check information valid');
    if (!Validations.isUserNameValid(userName)) {
      _userNameController.addError("Username is invalid");
      return false;
    }
    if (!Validations.isPasswordValid(password)) {
      _passwordController.addError("Password is invalid");
      return false;
    }
    if (!Validations.isPhoneValid(phone)) {
      _phoneController.addError("Phone number is invalid");
      return false;
    }
    if (!Validations.isEmailValid(email)) {
      _emailController.addError("Email is invalid");
      return false;
    }
    _userNameController.add("Username is valid");
    _passwordController.add("Password is valid");
    _phoneController.add("Phone numbe is valid");
    _emailController.add("Email is valid");
    print('isSignUpValid  ---    All information valid: ');
    return true;
  }

  bool isLoginValid(String email, String password) {
    if (!Validations.isEmailValid(email)) {
      _emailController.sink.addError("Email invalid");
      return false;
    }
    if (!Validations.isPasswordValid(password)) {
      _passwordController.sink.addError("Password must longer 4 characters");
      return false;
    }

    _emailController.sink.add("Email valid");
    _passwordController.sink.add("Password valid");
    return true;
  }

  void dispose() {
    _userNameController.close();
    _passwordController.close();
    _phoneController.close();
    _emailController.close();
  }
}
