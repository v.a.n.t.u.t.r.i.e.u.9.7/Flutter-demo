import 'package:flutter/material.dart';
import 'package:flutter_demo_01/blocs/auth_bloc.dart';
import 'package:flutter_demo_01/resources/screens/home.dart';
import 'package:flutter_demo_01/resources/screens/login.dart';

class SignUpPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SignUp(),
    );
  }
}

class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  AuthBloc _authBloc = new AuthBloc();

  TextEditingController _userNameController = new TextEditingController();
  TextEditingController _phoneController = new TextEditingController();
  TextEditingController _emailController = new TextEditingController();
  TextEditingController _passwordController = new TextEditingController();

  @override
  void dispose() {
    _authBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints.expand(),
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(left: 30, right: 30),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              buildContainerAnhThao(140),
              Container(
                child: Container(
                    child: Text("This is SIGN UP screen",
                        style:
                        TextStyle(fontSize: 22, color: Color(0xff323632)))),
              ),
              Container(
                padding: const EdgeInsets.only(bottom: 88),
                child: Text(
                  "Complete form to sign up",
                  style: TextStyle(fontSize: 16, color: Color(0xff606470)),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(bottom: 30),
                child: StreamBuilder(
                    stream: _authBloc.userNameStream,
                    builder: (context, snapshot) {
                      return TextField(
                          controller: _userNameController,
                          style: TextStyle(fontSize: 18, color: Colors.black),
                          decoration: InputDecoration(
                              errorText:
                              snapshot.hasError ? snapshot.error : null,
                              labelText: "Name",
                              prefixIcon: Container(
                                  width: 50, child: Icon(Icons.perm_identity)),
                              border: OutlineInputBorder(
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(6)))));
                    }),
              ),
              Container(
                margin: const EdgeInsets.only(bottom: 30),
                child: StreamBuilder(
                  stream: _authBloc.phoneStream,
                  builder: (context, snapshot) {
                    return TextField(
                        controller: _phoneController,
                        style:
                        TextStyle(fontSize: 16, color: Color(0xff323643)),
                        decoration: InputDecoration(
                            errorText:
                            snapshot.hasError ? snapshot.error : null,
                            labelText: "Phone number",
                            prefixIcon: Icon(Icons.phone_in_talk),
                            border: OutlineInputBorder(
                                borderRadius:
                                BorderRadius.all(Radius.circular(6)))));
                  },
                ),
              ),
              Container(
                margin: const EdgeInsets.only(bottom: 30),
                child: StreamBuilder(
                    stream: _authBloc.emailStream,
                    builder: (context, snapshot) {
                      return TextField(
                        controller: _emailController,
                        style:
                        TextStyle(fontSize: 16, color: Color(0xff323643)),
                        decoration: InputDecoration(
                            errorText:
                            snapshot.hasError ? snapshot.error : null,
                            labelText: "Email",
                            prefixIcon: Icon(Icons.email),
                            border: OutlineInputBorder(
                                borderRadius:
                                BorderRadius.all(Radius.circular(6)))),
                      );
                    }),
              ),
              Container(
                margin: const EdgeInsets.only(bottom: 30),
                child: StreamBuilder(
                    stream: _authBloc.passwordStream,
                    builder: (context, snapshot) {
                      return TextField(
                        controller: _passwordController,
                        style:
                        TextStyle(fontSize: 16, color: Color(0xff323643)),
                        decoration: InputDecoration(
                            errorText:
                            snapshot.hasError ? snapshot.error : null,
                            labelText: "Password",
                            prefixIcon: Icon(Icons.lock),
                            border: OutlineInputBorder(
                                borderRadius:
                                BorderRadius.all(Radius.circular(6)))),
                      );
                    }),
              ),
              Container(
                padding: const EdgeInsets.only(bottom: 40),
                child: SizedBox(
                  width: double.infinity,
                  height: 48,
                  child: RaisedButton(
                    onPressed: onSignUpClicked,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(6))),
                    color: Colors.blue,
                    child: Text("Sign up",
                        style: TextStyle(fontSize: 16, color: Colors.white)),
                  ),
                ),
              ),
              Container(
                width: double.infinity,
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 40),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "Already a User? ",
                        style:
                        TextStyle(fontSize: 16, color: Color(0xff606470)),
                      ),
                      GestureDetector(
                        onTap: onLoginClicked,
                        child: Text(
                          "Login now",
                          style:
                          TextStyle(fontSize: 16, color: Color(0xff3277d8)),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Container buildContainerAnhThao(double qqq) {
    return Container(
        margin: EdgeInsets.only(top: qqq),
        width: 192,
        height: 60,
        decoration: BoxDecoration(
            image: DecorationImage(
                image: new ExactAssetImage('asset/luffy_child.png'),
                fit: BoxFit.contain)));
  }

  void onLoginClicked() {
    Navigator.push(context, MaterialPageRoute(builder: loginPage));
  }

  Widget loginPage(BuildContext context) {
    return LoginPage();
  }

  void onSignUpClicked() {
    String userName = _userNameController.text;
    String phone = _phoneController.text;
    String email = _emailController.text;
    String password = _passwordController.text;

    print('---    : ');
    Navigator.push(context, MaterialPageRoute(builder: loginPage));
  }

  Widget homePage(BuildContext context) {
    return HomePage();
  }
}
