import "package:flutter/material.dart";
import 'package:flutter_demo_01/blocs/auth_bloc.dart';
import 'package:flutter_demo_01/resources/screens/sign_up.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Login(),
    );
  }
}

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final AuthBloc _authBloc = AuthBloc();
  TextEditingController emailController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();

  final welcomeAuthor = "Welcome demo flutter (11062019)";
  bool _isShowPassword = true;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              padding: EdgeInsets.fromLTRB(0, 170, 0, 30),
              child: Center(
                child: Image.asset(
                  "luffy_child.png",
                  width: 187.0,
                  height: 80,
                  fit: BoxFit.contain,
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.only(bottom: 10),
              child: Text(
                welcomeAuthor,
                style: TextStyle(fontSize: 22, color: Color(0xff323643)),
              ),
            ),
            Container(
              padding: const EdgeInsets.only(bottom: 145),
              child: Text(
                "Login to continue using app",
                style: TextStyle(fontSize: 16, color: Color(0xff6060470)),
              ),
            ),
            Container(
                padding: EdgeInsets.fromLTRB(30, 0, 30, 20),
                child: TextField(
                    controller: emailController,
                    decoration: InputDecoration(
                        labelText: "Email",
                        prefixIcon: Icon(Icons.email),
                        prefixStyle: TextStyle(color: Color(0xff606470)),
                        border: OutlineInputBorder(
                            borderRadius:
                            BorderRadius.all(Radius.circular(6)))))),
            Container(
              padding: const EdgeInsets.fromLTRB(30, 0, 30, 70),
              child: Stack(
                alignment: AlignmentDirectional.centerEnd,
                children: <Widget>[
                  TextField(
                      controller: passwordController,
                      obscureText: _isShowPassword,
                      decoration: InputDecoration(
                          labelText: "Password",
                          prefixIcon: Icon(Icons.lock),
                          prefixStyle: TextStyle(color: Color(0xff606470)),
                          border: OutlineInputBorder(
                              borderRadius:
                              BorderRadius.all(Radius.circular(6))))),
                  Container(
                    padding: EdgeInsets.only(right: 8),
                    child: GestureDetector(
                        onTap: onTapShowPassword,
                        child: Text(
                          _isShowPassword ? "Hide" : "Show",
                          style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                              color: Colors.green),
                        )),
                  )
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.fromLTRB(30, 0, 30, 40),
              child: SizedBox(
                width: double.infinity,
                height: 48,
                child: RaisedButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(6))),
                  color: Colors.blue,
                  onPressed: onLoginClicked,
                  child: Text(
                    "Log in",
                    style: TextStyle(
                        color: Color(0xfffbfbfb),
                        fontSize: 16,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            ),
            Container(
              width: double.infinity,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(30, 0, 30, 40),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "New User? ",
                      style: TextStyle(fontSize: 16, color: Color(0xff606470)),
                    ),
                    GestureDetector(
                      onTap: onSignUpClicked,
                      child: Text(
                        "Sign up for a new account",
                        style:
                        TextStyle(fontSize: 16, color: Color(0xff3277d8)),
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  void onLoginClicked() {
    print("object");
    String email, password;
    email = emailController.text;
    password = passwordController.text;

    bool isInformationValid = _authBloc.isLoginValid(email, password);

    print("onLoginClicked------------------isInformationValid----------------" +
        isInformationValid.toString());
  }

  void onSignUpClicked() {
    Navigator.push(context, MaterialPageRoute(builder: signUpPage));
  }

  Widget signUpPage(BuildContext context) {
    return SignUpPage();
  }

  void onTapShowPassword() {
    setState(() {
      _isShowPassword = !_isShowPassword;
    });
  }
}
