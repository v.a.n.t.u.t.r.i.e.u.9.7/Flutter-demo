import 'package:flutter/material.dart';

class LoadingDialog {
  static void showLoadingDialog(
      BuildContext context,
      String message,
      ) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) => Dialog(
          child: Container(
            color: Colors.white,
            height: 150,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[CircularProgressIndicator(), Text(message)],
            ),
          ),
        ));
  }

  static void hideLoadingDialog(BuildContext context) {
    Navigator.of(context).pop(showLoadingDialog);
  }
}
