import 'package:validators/validators.dart';

class Validations {
  static bool isUserNameValid(String userName) {
    return userName != null && userName.length > 6;
  }

  static bool isPhoneValid(String phone) {
    return isNumeric(phone) && phone.length > 9;
  }

  static bool isEmailValid(String email) {
    return email.length > 4 && email.contains("@");
  }

  static bool isPasswordValid(String password) {
    return password.length > 3;
  }
}
